import 'package:shared_preferences/shared_preferences.dart';
import 'package:weather_project/features/GetWeatherFeature/domain/entities/forcast_entity.dart'; 

abstract class IPrefsHelper {
  Future<SharedPreferences?> getPreferences();
  Future<int> getAppLanguage();
  Future<void> saveData(ForcastEntity forcastEntity);
  Future<ForcastEntity> getData();
  Future<bool> isStored(String? currentDate);
}
