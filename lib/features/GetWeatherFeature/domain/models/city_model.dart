import 'package:weather_project/features/GetWeatherFeature/domain/models/coord_model.dart';

class CityModel {
  String name;
  String country;
  int id;
  CoordModel coord;
  double population;
  double timezone;
  double sunset;
  double sunrise;

  CityModel({
    required this.name,
    required this.coord,
    required this.country,
    required this.timezone,
    required this.sunset,
    required this.id,
    required this.population,
    required this.sunrise,
  });

  factory CityModel.fromJson(Map<String, dynamic> json) {
    return CityModel(
        name: json['name'],
        country: json['country'],
        timezone: json['timezone'] * 1.0,
        id: json['id'],
        population: json['population'] * 1.0,
        sunrise: json['sunrise'] * 1.0,
        sunset: json['sunset'] * 1.0,
        coord: (json['coord'] != null ? CoordModel.fromJson(json['coord']) : {})
            as CoordModel);
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    data['country'] = country;
    data['timezone'] = timezone;
    data['id'] = id;
    data['population'] = population;
    data['sunrise'] = sunrise;
    data['sunset'] = sunset;
    data['coord'] = coord;
    return data;
  }

  static List<CityModel> fromJsonList(List<dynamic> items) {
     List<CityModel> list = [];
    for (var x in items) {
      list.add(CityModel.fromJson(x));
    }
    return list;
  }
}
