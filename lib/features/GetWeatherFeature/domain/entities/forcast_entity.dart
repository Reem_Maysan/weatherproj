import 'package:weather_project/features/GetWeatherFeature/domain/models/city_model.dart'; 
import 'package:weather_project/features/GetWeatherFeature/domain/models/main_model.dart';
import 'package:weather_project/features/GetWeatherFeature/domain/models/weather_model.dart';
import 'package:weather_project/features/GetWeatherFeature/domain/models/wind_model.dart';

class ForcastEntity {
  CityModel? city;
  WindModel? windModel;
  MainModel? mainModel;
  WeatherModel? weatherModel;
  String? currentDate;

  ForcastEntity({this.city, this.weatherModel, this.mainModel, this.windModel,this.currentDate});
}
