import 'package:weather_project/core/Response/response_model.dart';

abstract class WeatherIHttpHelper {
  Future<ResponseModel> getForcastDataList();
}
