const Map<String, String> imagesNames = {
  'firstPage': 'assets/images/first-page.jpg',
  'sunny': 'assets/images/sun.jpg',
  'windy': 'assets/images/wind.jpg',
  'cloudy': 'assets/images/cloud.jpg',
  'sunCloud': 'assets/images/sun-cloud.jpg',
  'splash': 'assets/images/splash.jpg',
  'arrow': 'assets/images/arrow.png',
  'loading': 'assets/images/loading.gif',
};
