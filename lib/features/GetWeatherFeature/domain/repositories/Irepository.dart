 
import 'package:weather_project/core/Response/returned_response.dart';
import 'package:weather_project/features/GetWeatherFeature/domain/models/basic_response_model.dart';
import 'package:weather_project/features/GetWeatherFeature/domain/models/forcast_model.dart';

abstract class WeatherIRepository {
  Future<ReturnedResponse> getForcastDataList(String? date);
  Future<void> saveData(BasicResponseModel basicResponseModel,ForcastModel forcastModel,String date);
}
