class CoordModel {
  double lat;
  double lon;

  CoordModel({
    required this.lat,
    required this.lon,
  });

  factory CoordModel.fromJson(Map<String, dynamic> json) {
    return CoordModel(lat: json['lat'], lon: json['lon']);
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['lat'] = lat;
    data['lon'] = lon;
    return data;
  }

  static List<CoordModel> fromJsonList(List<dynamic> items) {
     List<CoordModel> list = [];
    for (var x in items) {
      list.add(CoordModel.fromJson(x));
    }
    return list;
  }
}
