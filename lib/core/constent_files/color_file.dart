import 'dart:ui';

import 'package:flutter/material.dart';

class ColorFile {      
  static const Color loadingColor = Color(0xFF69bdee); 
  static const Color greenDateColor = Color(0xFF3c8f7d); 
  static const Color blueLoaderColor = Color(0xFF7d97f4); 
  static const Color backgroundColor = Color(0xFFbeecf9); 
  static const Color backgroundAddColor = Color(0xFFCBCBCB);
  static const Color whiteColor = Color(0xFFFFFFFF);
  static const Color pinkColor = Color(0xFFFD7C9B);
  static const Color yellowColor = Color(0xFFFED39F);
  static const Color blackColor = Color(0xFF444444);
  static const Color greyFirstColor = Color(0xFFAAAAAA);
  static const Color grey10econdColor = Color(0xFFB6B6B6);
  static const Color greyThirdColor = Color(0xFFBBBBBB);
  static const Color greyFourthColor = Color(0xFFE8E8E8);

  static const Color darkBlueColor = Color(0xFF000B3F);
  static const Color greyTextFieldColor = Color(0xFFBBBBBB);
  static const Color whiteTextFieldColor = Color(0xFFF5F6F8); 
 
}
