import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart'; 

class WeatherCard extends StatelessWidget {
  final String? title, value;
  const WeatherCard({
    Key? key,
    this.title,
    this.value,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(const BoxConstraints(), context: context);
    return Container(
      width: ScreenUtil().setWidth(100), 
      padding: const EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(title!,
              textAlign: TextAlign.start,
              style: const TextStyle(
                fontSize: 14,
                color: Colors.black, 
              )),
          Text(value!,
              textAlign: TextAlign.start,
              style: const TextStyle(
                  fontSize: 16,
                  color: Colors.black, 
                  fontWeight: FontWeight.bold)),
        ],
      ),
    );
  }
}
