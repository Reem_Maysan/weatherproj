import 'package:bloc/bloc.dart';
import 'package:weather_project/app/domain/repositories/Irepository.dart';
import 'package:weather_project/core/localization/app_language.dart';
import 'app_event.dart';
import 'app_state.dart';

class AppBloc extends Bloc<AppEvent, AppState> {
  AppIRepository? appIRepository;
  AppBloc({this.appIRepository})
      : super(const AppState(
          appLanguage: 0,
        )) {
    on<InitEvent>((event, emit) async {
      emit(const AppState(
        appLanguage: 0,
      ));
      final language = await appIRepository!.getAppLanguage();
      emit(AppState(
        appLanguage: language,
      ));
      changeAppLanguage(language);
    });
  }

  // @override
  // Stream<AppState> mapEventToState(
  //   AppEvent event,
  // ) async* {
  //   if (event is InitEvent) {
  //     yield const AppState(
  //       appLanguage: 0,
  //     );
  //     final language = await appIRepository.getAppLanguage();
  //     yield AppState(
  //       appLanguage: language,
  //     );
  //     changeAppLanguage(language);
  //   }
  // }
}
