class WeatherModel {
  int id;
  String main;
  String description;
  String icon;

  WeatherModel({
    required this.description,
    required this.icon,
    required this.id,
    required this.main,
  });

  factory WeatherModel.fromJson(Map<String, dynamic> json) {
    return WeatherModel(
        description: json['description'],
        id: json['id'],
        main: json['main'],
        icon: json['icon']);
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['description'] = description;
    data['id'] = id;
    data['main'] = main;
    data['icon'] = icon;
    return data;
  }

  static List<WeatherModel> fromJsonList(List<dynamic>? items) {
    items ?? [];
    List<WeatherModel> list = [];
    for (var x in items!) {
      list.add(WeatherModel.fromJson(x));
    }
    return list;
  }
}
