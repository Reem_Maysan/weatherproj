import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart'; 

Future<bool?> error(String errorCode) {
  return Fluttertoast.showToast(
      msg: errorCode,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      backgroundColor: Colors.red,
      textColor: Colors.white,
      fontSize: 16.0);
}

Future<bool?> success(String successCode) {
  return Fluttertoast.showToast(
      msg: successCode,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      backgroundColor: Colors.green,
      textColor: Colors.white,
      fontSize: 16.0);
}
