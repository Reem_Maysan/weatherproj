 
import 'package:shared_preferences/shared_preferences.dart';
import 'package:weather_project/core/constent_files/constent.dart';
import 'package:weather_project/core/localization/app_language.dart';
import 'package:weather_project/core/preference/Ipreference_helper.dart';
import 'package:weather_project/features/GetWeatherFeature/domain/entities/forcast_entity.dart'; 
import 'package:weather_project/features/GetWeatherFeature/domain/models/city_model.dart';
import 'package:weather_project/features/GetWeatherFeature/domain/models/coord_model.dart';
import 'package:weather_project/features/GetWeatherFeature/domain/models/main_model.dart';
import 'package:weather_project/features/GetWeatherFeature/domain/models/weather_model.dart';
import 'package:weather_project/features/GetWeatherFeature/domain/models/wind_model.dart';
import 'package:weather_project/injection.dart';

class PrefsHelper implements IPrefsHelper {
  final SharedPreferences? sharedPreferences = sl<SharedPreferences>();

  @override
  Future<SharedPreferences?> getPreferences() async {
    return sharedPreferences;
  }

  @override
  Future<int> getAppLanguage() async {
    return (await getPreferences())!.getInt(APP_LANGUAGE) ?? AppLanguageKeys.EN;
  }

  @override
  Future<void> saveData(ForcastEntity forcastEntity) async {
    (await getPreferences())!.clear();
    sharedPreferences!.setString(CurrentDate, forcastEntity.currentDate!);
    sharedPreferences!.setString(CityName, forcastEntity.city!.name);
    sharedPreferences!.setString(ImageMain, forcastEntity.weatherModel!.main);
    sharedPreferences!
        .setString(Description, forcastEntity.weatherModel!.description);
    sharedPreferences!.setDouble(Humidity, forcastEntity.mainModel!.humidity);
    sharedPreferences!
        .setDouble(Temperature, forcastEntity.mainModel!.temperature);
    sharedPreferences!.setDouble(Wind, forcastEntity.windModel!.speed);
  }

  @override
  Future<bool> isStored(String? currentDate) async {
    String? date = (await getPreferences())!.getString(CurrentDate);
    return date != null && date == currentDate;
  }

  @override
  Future<ForcastEntity> getData() async {
    String image =
        (await getPreferences())!.getString(ImageMain) ?? "ImageMain";
    String currentDate =
        (await getPreferences())!.getString(CurrentDate) ?? "CurrentDate";
    String cityName =
        (await getPreferences())!.getString(CityName) ?? "CityName";
    String description =
        (await getPreferences())!.getString(Description) ?? "Description";
    double humidity = (await getPreferences())!.getDouble(Humidity) ?? 0;
    double temperature =
        (await getPreferences())!.getDouble(Temperature) ?? 0.0;
    double wind = (await getPreferences())!.getDouble(Wind) ?? 0.0;
    ForcastEntity forcastEntity = ForcastEntity(
        currentDate: currentDate,
        city: CityModel(
            name: cityName,
            coord: CoordModel(lat: 0.0, lon: 0.0),
            country: '',
            id: 0,
            population: 0.0,
            sunrise: 0.0,
            sunset: 0.0,
            timezone: 0.0),
        weatherModel: WeatherModel(
            description: description, icon: '', id: 0, main: image),
        mainModel: MainModel(
            feelsLike: 0.0,
            groundLevel: 0.0,
            pressure: 0.0,
            seaLevel: 0.0,
            temperatureKf: 0.0,
            temperatureMax: 0.0,
            temperatureMin: 0.0,
            humidity: humidity,
            temperature: temperature),
        windModel: WindModel(degree: 0.0, gust: 0.0, speed: wind));

    return forcastEntity;
  }
}
