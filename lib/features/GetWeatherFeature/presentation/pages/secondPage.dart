import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:weather_project/core/constent_files/color_file.dart';
import 'package:weather_project/core/constent_files/images_file.dart';
import 'package:weather_project/core/core_functions.dart';
import 'package:weather_project/features/GetWeatherFeature/domain/models/cloud_model.dart';
import 'package:weather_project/features/GetWeatherFeature/domain/models/forcast_model.dart';
import 'package:weather_project/features/GetWeatherFeature/domain/models/main_model.dart';
import 'package:weather_project/features/GetWeatherFeature/domain/models/system_model.dart';
import 'package:weather_project/features/GetWeatherFeature/domain/models/wind_model.dart';
import 'package:weather_project/features/GetWeatherFeature/presentation/bloc/bloc.dart';
import 'package:weather_project/features/GetWeatherFeature/presentation/widgets/helper_functions.dart';
import 'package:weather_project/features/GetWeatherFeature/presentation/widgets/page_slider.dart';
import 'package:weather_project/features/GetWeatherFeature/presentation/widgets/weather_card.dart';

class SecondPage extends StatefulWidget {
  final DateTime date;
  const SecondPage({Key? key, required this.date}) : super(key: key);

  @override
  _SecondPageState createState() => _SecondPageState();
}

class _SecondPageState extends State<SecondPage> {
  late var bloc;
  List<ForcastModel> forcastModelList = [];
  List<WeatherCard> weatherCards = [];
  late ForcastModel forcastModel;
  late String image;

  late String cityName, description;
  late double temperature, wind, humidity;
  //int humidity;
  @override
  void initState() {
    bloc = BlocProvider.of<WeatherBloc>(context);
    bloc.add(FetchForcastDataList(date: widget.date.toString()));
    cityName = '';
    description = '';
    temperature = 0.0;
    wind = 0.0;
    humidity = 0;
    image = imagesNames['sunny']!;
    forcastModel = ForcastModel(
        weatherModel: [],
        mainModel: MainModel(
            feelsLike: 0.0,
            groundLevel: 0.0,
            humidity: 0.0,
            pressure: 0.0,
            seaLevel: 0.0,
            temperature: 0.0,
            temperatureKf: 0.0,
            temperatureMax: 0.0,
            temperatureMin: 0.0),
        windModel: WindModel(degree: 0.0, gust: 0.0, speed: 0.0),
        systemModel: SystemModel(pod: ''),
        cloudModel: CloudModel(all: 0.0),
        dateNumber: 0,
        dateText: '',
        pod: '',
        pop: 0,
        visibility: 0);
    super.initState();
  }

  PageController pageController = PageController(viewportFraction: 0.6);

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(const BoxConstraints(), context: context);
    return BlocListener<WeatherBloc, WeatherStates>(listener: (context, state) {
      if (state is Error) {
        error(state.error!);
      } else if (state is ForcastLoaded) {
        if (state.basicResponseModel != null) {
          forcastModel = chooseForcastModel(
              state.basicResponseModel!.list, widget.date.toString());

          cityName = state.basicResponseModel!.city.name;
          description = forcastModel.weatherModel.isNotEmpty
              ? forcastModel.weatherModel.first.description
              : '';
          temperature = forcastModel.mainModel.temperature;
          humidity = forcastModel.mainModel.humidity;
          wind = forcastModel.windModel.speed;
          image = chooseImage(forcastModel.weatherModel.isNotEmpty
              ? forcastModel.weatherModel.first.main
              : 'Clear');
        } else {
          cityName = state.forcastEntity!.city!.name;
          description = state.forcastEntity!.weatherModel!.description;
          temperature = state.forcastEntity!.mainModel!.temperature;
          humidity = state.forcastEntity!.mainModel!.humidity;
          wind = state.forcastEntity!.windModel!.speed;
          image = chooseImage(state.forcastEntity!.weatherModel!.main);
        }
        weatherCards = initializeWeatherCardsList(
            (wind.round()).toString(), (humidity.round()).toString());
        setState(() {});
      }
    }, child:
        BlocBuilder<WeatherBloc, WeatherStates>(builder: (context, state) {
      return SafeArea(
          child: Scaffold(
              backgroundColor: ColorFile.loadingColor,
              body: state is WeatherLoading
                  ? Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          alignment: Alignment.center,
                          padding: const EdgeInsets.only(bottom: 50),
                          child: Image(
                            width: ScreenUtil().setWidth(300),
                            fit: BoxFit.contain,
                            image: AssetImage(imagesNames['loading']!),
                          ),
                        ),
                        const Text(
                          'Loading..',
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 20, color: Colors.white),
                        ),
                      ],
                    )
                  : Container(
                      color: ColorFile.backgroundColor,
                      padding: const EdgeInsets.only(
                          left: 20, top: 20, bottom: 10, right: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            child: Container(
                              height: double.infinity,
                              width: ScreenUtil().setWidth(180),
                              child: Text(description,
                                  textAlign: TextAlign.start,
                                  style: const TextStyle(
                                    fontSize: 50,
                                    color: Colors.white,
                                    //fontFamily: FONT_FAMILY,
                                  )),
                            ),
                          ),
                          Container(
                            alignment: Alignment.center,
                            padding: const EdgeInsets.only(top: 10),
                            child: Image(
                              fit: BoxFit.contain,
                              image: AssetImage(image),
                            ),
                          ),
                          const Spacer(),
                          Text(cityName,
                              textAlign: TextAlign.start,
                              style: const TextStyle(
                                fontSize: 40,
                                color: Colors.white,
                                //    fontFamily: FONT_FAMILY,
                              )),
                          SizedBox(
                            height: ScreenUtil().setHeight(100),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Expanded(
                                  child: Container(
                                    padding: const EdgeInsets.only(
                                        left: 20, right: 10),
                                    height: double.infinity,
                                    width: ScreenUtil().setWidth(40),
                                    child: FittedBox(
                                      fit: BoxFit.fitHeight,
                                      child: Text(
                                          (temperature.round()).toString() +
                                              "\u00B0",
                                          textAlign: TextAlign.start,
                                          style: const TextStyle(
                                              //fontSize: 40,
                                              color: Colors.white,
                                              //  fontFamily: FONT_FAMILY,
                                              fontWeight: FontWeight.bold)),
                                    ),
                                  ),
                                ),
                                const SizedBox(width: 10),
                                Expanded(
                                  child: PageSlider(
                                      key: widget.key,
                                      height: ScreenUtil().setHeight(80),
                                      weatherCardsList: weatherCards),
                                ),
                              ],
                            ),
                          )
                        ],
                      ))));
    }));
  }
}
