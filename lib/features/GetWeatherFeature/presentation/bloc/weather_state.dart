import 'package:equatable/equatable.dart';
import 'package:weather_project/features/GetWeatherFeature/domain/entities/forcast_entity.dart';
import 'package:weather_project/features/GetWeatherFeature/domain/models/basic_response_model.dart'; 

abstract class WeatherStates extends Equatable {}

class WeatherInitialState extends WeatherStates {
  @override
  List<Object> get props => [];
}

class WeatherLoading extends WeatherStates {
  WeatherLoading();
  @override
  List<Object> get props => [];
}

class ForcastLoading extends WeatherStates {
  ForcastLoading();
  @override
  List<Object> get props => [];
}

class ForcastLoaded extends WeatherStates {
  final BasicResponseModel? basicResponseModel;
  final ForcastEntity? forcastEntity;
  ForcastLoaded({this.basicResponseModel, this.forcastEntity});
  @override
  List<Object?> get props => [basicResponseModel, forcastEntity];
}

class Error extends WeatherStates {
  final String? error;
  Error({this.error});
  @override
  List<Object?> get props => [error];
}
