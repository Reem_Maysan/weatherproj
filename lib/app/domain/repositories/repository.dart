import 'package:weather_project/app/domain/repositories/Irepository.dart';
import 'package:weather_project/core/preference/Ipreference_helper.dart';

class AppRepository implements AppIRepository {
  IPrefsHelper? iPrefsHelper;
  AppRepository(this.iPrefsHelper);

  @override
  Future<int> getAppLanguage() async {
    return await iPrefsHelper!.getAppLanguage();
  }
}
