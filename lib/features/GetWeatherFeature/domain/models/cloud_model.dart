class CloudModel {
  double all;

  CloudModel({
    required this.all,
  });

  factory CloudModel.fromJson(Map<String, dynamic> json) {
    return CloudModel(all: json['all'] * 1.0);
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['all'] = all;
    return data;
  }

  static List<CloudModel> fromJsonList(List<dynamic> items) {
     List<CloudModel> list = [];
    for (var x in items) {
      list.add(CloudModel.fromJson(x));
    }
    return list;
  }
}
