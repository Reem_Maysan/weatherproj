import 'package:flutter/material.dart';
import 'package:weather_project/core/constent_files/color_file.dart'; 
import 'package:weather_project/core/constent_files/images_file.dart';

class DateCard extends StatelessWidget {
  final String? title, value;
  const DateCard({Key? key, this.title, this.value}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: ColorFile.greenDateColor,
          borderRadius: BorderRadius.circular(10)),
      padding: const EdgeInsets.only(bottom: 5, top: 5, left: 10,right:10),
      child: Row(
        children: [
          Image(
            image: AssetImage(imagesNames['arrow']!),
            fit: BoxFit.cover,
            width: 30,
          ),
         const Spacer(),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 15),
                child: Text(title!,
                    textAlign: TextAlign.end,
                    style: const TextStyle(
                        fontSize: 15,
                        color: Colors.black, 
                        fontWeight: FontWeight.bold)),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Text(value!,
                    textAlign: TextAlign.end,
                    style: const TextStyle(
                      fontSize: 14,
                      color: Colors.black, 
                    )),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
