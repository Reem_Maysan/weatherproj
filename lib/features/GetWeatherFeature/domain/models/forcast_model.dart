import 'package:weather_project/features/GetWeatherFeature/domain/models/cloud_model.dart';
import 'package:weather_project/features/GetWeatherFeature/domain/models/main_model.dart';
import 'package:weather_project/features/GetWeatherFeature/domain/models/system_model.dart';
import 'package:weather_project/features/GetWeatherFeature/domain/models/weather_model.dart';
import 'package:weather_project/features/GetWeatherFeature/domain/models/wind_model.dart';

class ForcastModel {
  int dateNumber;
  String dateText, pod;
  MainModel mainModel;
  List<WeatherModel> weatherModel;
  CloudModel cloudModel;
  WindModel windModel;
  int visibility, pop;
  SystemModel systemModel;
  ForcastModel(
      {required this.dateText,
      required this.mainModel,
      required this.cloudModel,
      required this.dateNumber,
      required this.systemModel,
      required this.visibility,
      required this.windModel,
      required this.pod,
      required this.pop,
      required this.weatherModel});

  factory ForcastModel.fromJson(Map<String, dynamic> json) {
    return ForcastModel(
      dateNumber: json['dt'],
      dateText: json['dt_txt'],
      mainModel: (json['main'] != null ? MainModel.fromJson(json['main']) : {})
          as MainModel,
      weatherModel: (json['weather'] != null
          ? WeatherModel.fromJsonList(json['weather'])
          : []) as List<WeatherModel>,
      cloudModel: (json['clouds'] != null
          ? CloudModel.fromJson(json['clouds'])
          : {}) as CloudModel,
      windModel: (json['wind'] != null ? WindModel.fromJson(json['wind']) : {})
          as WindModel,
      visibility: json['visibility'],
      pop: json['pop'] ?? 0,
      pod: json['pod'] ?? '',
      systemModel: (json['sys'] != null
          ? SystemModel.fromJson(json['sys'])
          : {}) as SystemModel,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['dt_txt'] = dateText;
    data['dt'] = dateNumber;
    data['main'] = mainModel;
    data['weather'] = weatherModel;
    data['clouds'] = cloudModel;
    data['wind'] = windModel;
    data['visibility'] = visibility;
    data['pop'] = pop;
    data['pod'] = pod;
    data['sys'] = systemModel;
    return data;
  }

  static List<ForcastModel> fromJsonList(List<dynamic>? items) {
    items ?? [];
    List<ForcastModel> list = [];
    for (var x in items!) {
      list.add(ForcastModel.fromJson(x));
    }
    return list;
  }
}
