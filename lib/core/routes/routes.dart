import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_project/app/presentation/app.dart';
import 'package:weather_project/core/routes/routesNames.dart';
import 'package:weather_project/features/GetWeatherFeature/presentation/bloc/bloc.dart';
import 'package:weather_project/features/GetWeatherFeature/presentation/pages/first_page.dart';
import 'package:weather_project/features/GetWeatherFeature/presentation/pages/secondPage.dart';
import 'package:weather_project/features/SplashScreen/prestentation/pages/basicSplash.dart';
import 'package:weather_project/injection.dart';

class RouteGenerator {
  Route<dynamic> ongenerate(RouteSettings value) {
    WeatherBloc? weatherBloc = sl<WeatherBloc>();
    String? name = value.name;
    final args = value.arguments;
    switch (name) {
      case RoutesNames.AppPage:
        {
          return MaterialPageRoute(builder: (context) => const App());
        }
      case RoutesNames.BasicSplash:
        {
          return MaterialPageRoute(
              builder: (context) => const BasicSplashScreen());
        }
      case RoutesNames.FirstPage:
        {
          return MaterialPageRoute(
              builder: (context) => BlocProvider<WeatherBloc>.value(
                  value: weatherBloc, child: const FirstPage()));
        }
      case RoutesNames.SecondPage:
        {
          return MaterialPageRoute(
              builder: (context) => BlocProvider<WeatherBloc>.value(
                  value: weatherBloc,
                  child: SecondPage(
                    date: args as DateTime,
                  )));
        }

      default:
        return MaterialPageRoute(
            builder: (context) => const BasicSplashScreen());
    }
  }
}
