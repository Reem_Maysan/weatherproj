import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:weather_project/app/presentation/bloc/app_bloc.dart';
import 'package:weather_project/core/localization/app_language.dart';
import 'package:weather_project/core/localization/app_localizations.dart';
import 'package:weather_project/core/routes/routes.dart';
import 'package:weather_project/core/routes/routesNames.dart';
import 'package:weather_project/features/GetWeatherFeature/presentation/pages/first_page.dart';
import 'package:weather_project/features/SplashScreen/prestentation/pages/basicSplash.dart';
import 'package:weather_project/injection.dart';
import 'bloc/app_event.dart';
import 'bloc/app_state.dart';

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  final AppBloc? appBloc = sl<AppBloc>();
  late RouteGenerator routeGenerator;

  final GlobalKey<NavigatorState>? myAppKey = sl<GlobalKey<NavigatorState>>();
  @override
  void initState() {
    appBloc!.add(const InitEvent());
    routeGenerator = RouteGenerator();
    super.initState();
  }

  @override
  void dispose() {
    appBloc!.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
        bloc: appBloc,
        builder: (BuildContext context, AppState state) { 
          return StreamBuilder(
              stream: localeSubjectAppLanguage.stream.distinct(),
              initialData: state.appLanguage == AppLanguageKeys.AR
                  ? const Locale('ar', '')
                  : const Locale('en', ''),
              builder: (context, snapshotLanguage) {
                return MaterialApp(
                    navigatorKey: myAppKey,
                    onGenerateRoute: routeGenerator.ongenerate,
                    debugShowCheckedModeBanner: false,
                    title: "WeatherApp",
                    //    initialRoute: RoutesNames.BasicSplash,
                    home: const BasicSplashScreen(),
                    locale: snapshotLanguage.data == AppLanguageKeys.AR
                        ? const Locale('ar', '')
                        : const Locale('en', ''),
                    localizationsDelegates: const [
                      AppLocalizations.delegate,
                      GlobalMaterialLocalizations.delegate,
                      GlobalWidgetsLocalizations.delegate,
                      DefaultCupertinoLocalizations.delegate,
                    ],
                    supportedLocales: const [
                      Locale('en', ''),
                      Locale('ar', ''),
                    ]);
              });
        });
  }
}
