 
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_project/features/GetWeatherFeature/domain/repositories/Irepository.dart';
import 'package:weather_project/features/GetWeatherFeature/presentation/bloc/weather_event.dart';
import 'package:weather_project/features/GetWeatherFeature/presentation/bloc/weather_state.dart';
import 'package:weather_project/features/GetWeatherFeature/presentation/widgets/helper_functions.dart';

class WeatherBloc extends Bloc<WeatherEvents, WeatherStates> {
  final WeatherIRepository repository;

  WeatherBloc({
    required this.repository,
  }) : super(WeatherInitialState()) {
    on<FetchForcastDataList>((event, emit) async {
      emit(WeatherLoading()); 
      final result = await repository.getForcastDataList(event.date); 
      if (result.isStored) {
        emit(ForcastLoaded(
            forcastEntity: result.forcastEntity, basicResponseModel: null));
      } else {
        if (result.responseModel.hasError) { 
          emit(Error(error:'Something went wrong!'));
        } else { 
          await repository.saveData(
              result.responseModel.model,
              chooseForcastModel(result.responseModel.model!.list, event.date),
              event.date);
          emit(ForcastLoaded(
              basicResponseModel: result.responseModel.model,
              forcastEntity: null)); 
        }
      }
    });
  }
}
