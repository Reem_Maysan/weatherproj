import 'package:equatable/equatable.dart';

abstract class AppEvent extends Equatable {
  const AppEvent();
}

class InitEvent extends AppEvent {
  const InitEvent();

  @override
  List<Object> get props => [];
}
