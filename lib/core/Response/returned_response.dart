import 'package:weather_project/core/Response/response_model.dart';
import 'package:weather_project/features/GetWeatherFeature/domain/entities/forcast_entity.dart';

class ReturnedResponse<T> {
  late ResponseModel responseModel;
  ForcastEntity? forcastEntity;

  bool get isStored => forcastEntity != null;
  ReturnedResponse(dynamic response) {
    if (response is ForcastEntity) {
      forcastEntity = response;
    } else {
      responseModel = response;
    }
  }
}
