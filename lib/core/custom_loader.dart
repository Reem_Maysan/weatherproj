import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart'; 

Widget customLoader(Color color) {
  return SpinKitChasingDots(
    color: color,
    size: 50.0,
  );
}
