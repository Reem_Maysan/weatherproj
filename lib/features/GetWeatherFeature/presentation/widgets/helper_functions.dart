import 'package:weather_project/core/constent_files/images_file.dart';
import 'package:weather_project/features/GetWeatherFeature/domain/models/forcast_model.dart';
import 'package:weather_project/features/GetWeatherFeature/presentation/widgets/weather_card.dart';

ForcastModel chooseForcastModel(
    List<ForcastModel> forcastModelList, String currentDate) {
  int splitIndex = currentDate.indexOf(" ");
  String date = currentDate.substring(0, splitIndex);
  for (int i = 0; i < forcastModelList.length; i++) {
    int splitIndex2 = forcastModelList[i].dateText.indexOf(" ");
    String date2 = forcastModelList[i].dateText.substring(0, splitIndex2);
    if (date == date2) {
      return forcastModelList[i];
    }
  }
  return forcastModelList[0];
}

List<WeatherCard> initializeWeatherCardsList(
    String windSpeed, String humidity) {
  List<WeatherCard> list = [];
  list.add(WeatherCard(
    title: 'Wind',
    value: windSpeed + 'km/h',
  ));
  list.add(WeatherCard(
    title: 'Humidity',
    value: humidity + '%',
  ));
  return list;
}

String chooseImage(String weather) {
  switch (weather) {
    case 'Clear':
      return imagesNames['sunCloud']!;
    case 'Clouds':
      return imagesNames['cloudy']!;
    default:
      return imagesNames['sunny']!;
  }
}
