import 'dart:async'; 
import 'package:flutter/material.dart';
import 'package:weather_project/core/constent_files/color_file.dart'; 
import 'package:weather_project/core/constent_files/images_file.dart'; 
import 'package:weather_project/core/routes/routesNames.dart';

class BasicSplashScreen extends StatefulWidget {
  const BasicSplashScreen({Key? key}) : super(key: key);

  @override
  _BasicSplashScreenState createState() => _BasicSplashScreenState();
}

class _BasicSplashScreenState extends State<BasicSplashScreen> {
  @override
  void initState() {
    super.initState();

    Timer(
        const Duration(seconds: 5),
        () => Navigator.of(context).pushNamedAndRemoveUntil(
              RoutesNames.FirstPage,
              (route) => false,
            ));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Container(
            width: double.infinity,
            height: double.infinity,
            constraints: const BoxConstraints.expand(),
            alignment: Alignment.center,
            color: Colors.white,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const Spacer(),
                Padding(
                    padding: const EdgeInsets.only(bottom: 10.0),
                    child: Image(
                        image: AssetImage(imagesNames['splash']!),
                        fit: BoxFit.contain)),
                const Spacer(),
                Padding(
                  padding: const EdgeInsets.only(bottom: 10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Text(
                        'Hello Weather!',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 30,
                            fontWeight: FontWeight.bold,
                            color: ColorFile.blueLoaderColor),
                      ),
                      Text(
                        'An application where you can get your weather state',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 13, color: ColorFile.blueLoaderColor),
                      ),
                    ],
                  ),
                ),
              ],
            )),
      ),
    );
  }
}
