import 'package:weather_project/features/GetWeatherFeature/domain/models/city_model.dart';
import 'package:weather_project/features/GetWeatherFeature/domain/models/forcast_model.dart';

class BasicResponseModel {
  String cod;
  int message, cnt;
  CityModel city;
  List<ForcastModel> list;

  BasicResponseModel({
    required this.city,
    required this.cnt,
    required this.list,
    required this.cod,
    required this.message,
  });

  factory BasicResponseModel.fromJson(Map<String, dynamic> json) {
    return BasicResponseModel(
      city: (json['city'] != null ? CityModel.fromJson(json['city']) : {})
          as CityModel,
      cnt: json['cnt'],
      cod: json['cod'],
      message: json['message'],
      list: (json['list'] != null
          ? ForcastModel.fromJsonList(json['list'])
          : []) as List<ForcastModel>,
    );
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['city'] = city;
    data['cnt'] = cnt;
    data['cod'] = cod;
    data['message'] = message;
    data['list'] = list;
    return data;
  }

  static List<BasicResponseModel> fromJsonList(List<dynamic> items) {
    List<BasicResponseModel> list = [];
    for (var x in items) {
      list.add(BasicResponseModel.fromJson(x));
    }
    return list;
  }
}
