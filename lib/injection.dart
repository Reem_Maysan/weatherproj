import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:weather_project/app/domain/repositories/Irepository.dart';
import 'package:weather_project/app/domain/repositories/repository.dart';
import 'package:weather_project/core/constent_files/constent.dart';
import 'package:weather_project/core/preference/Ipreference_helper.dart';
import 'package:weather_project/core/preference/preference_helper.dart';
import 'package:weather_project/features/GetWeatherFeature/data/http/Ihttp_helper.dart';
import 'package:weather_project/features/GetWeatherFeature/data/http/http_helper.dart';
import 'package:weather_project/features/GetWeatherFeature/domain/repositories/Irepository.dart';
import 'package:weather_project/features/GetWeatherFeature/domain/repositories/repository.dart';

import 'app/presentation/bloc/app_bloc.dart';
import 'features/GetWeatherFeature/presentation/bloc/bloc.dart';

final sl = GetIt.instance;

Future iniGetIt() async {
  sl.registerLazySingleton(() => Dio(BaseOptions(
      baseUrl: BaseUrl, 
      validateStatus: (status) {
        return status! < 500;
      },
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        "charset": "utf-8",
        "Accept-Charset": "utf-8"
      },
      responseType: ResponseType.plain)));

  sl.registerLazySingleton<IPrefsHelper>(() => PrefsHelper());

  sl.registerLazySingleton<WeatherIHttpHelper>(() => WeatherHttpHelper(sl()));
  sl.registerLazySingleton<WeatherIRepository>(
      () => WeatherRepository(sl(), sl()));

  sl.registerLazySingleton<AppIRepository>(() => AppRepository(
        sl(),
      ));

  ///Blocs
  sl.registerFactory(() => WeatherBloc(repository: sl()));
  sl.registerFactory(() => AppBloc(appIRepository: sl()));

  //SharedPreferences
  SharedPreferences sharedPreference = await SharedPreferences.getInstance();
  sl.registerLazySingleton<SharedPreferences>(() => sharedPreference);

  GlobalKey<NavigatorState> globalKey = GlobalKey<NavigatorState>();
  sl.registerLazySingleton<GlobalKey<NavigatorState>>(() => globalKey);
}
