class SystemModel {
  String pod;

  SystemModel({
    required this.pod,
  });

  factory SystemModel.fromJson(Map<String, dynamic> json) {
    return SystemModel(pod: json['pod']);
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['pod'] = pod;
    return data;
  }

  static List<SystemModel> fromJsonList(List<dynamic> items) {
     List<SystemModel> list = [];
    for (var x in items) {
      list.add(SystemModel.fromJson(x));
    }
    return list;
  }
}
