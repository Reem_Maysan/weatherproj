import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart'; 
import 'package:weather_project/core/constent_files/images_file.dart';
import 'package:weather_project/core/core_functions.dart'; 
import 'package:weather_project/core/routes/routesNames.dart'; 
import 'package:weather_project/features/GetWeatherFeature/presentation/bloc/bloc.dart';
import 'package:weather_project/features/GetWeatherFeature/presentation/widgets/date_card.dart'; 
import 'package:weather_project/features/GetWeatherFeature/presentation/widgets/weather_card.dart'; 

class FirstPage extends StatefulWidget {
  const FirstPage({Key? key}) : super(key: key);

  @override
  _FirstPageState createState() => _FirstPageState();
}

class _FirstPageState extends State<FirstPage> {
  var bloc;

  List<WeatherCard> weatherCardsList = [];

  DateTime? todayDate, tomorrowDate, afterTomorrowDate;
  @override
  void initState() {
    bloc = BlocProvider.of<WeatherBloc>(context);
    final now = DateTime.now();
    todayDate = DateTime(
        now.year, now.month, now.day, now.hour, now.minute, now.second);
    tomorrowDate = DateTime(
        now.year, now.month, now.day + 1, now.hour, now.minute, now.second);
    afterTomorrowDate = DateTime(
        now.year, now.month, now.day + 2, now.hour, now.minute, now.second);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(const BoxConstraints(), context: context);
    return BlocListener<WeatherBloc, WeatherStates>(listener: (context, state) {
      if (state is Error) { 
        error(state.error!);
      }
    }, child:
        BlocBuilder<WeatherBloc, WeatherStates>(builder: (context, state) {
      return SafeArea(
          child: Scaffold(
              backgroundColor: Colors.white,
              body: state is WeatherLoading
                  ? Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          alignment: Alignment.center,
                          padding: const EdgeInsets.only(bottom: 20),
                          child: Image(
                            width: ScreenUtil().setWidth(300),
                            fit: BoxFit.contain,
                            image: AssetImage(imagesNames['loading']!),
                          ),
                        ),
                        const Spacer(),
                        const Text(
                          'Loading..',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              //fontFamily: FONT_FAMILY,
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.white),
                        ),
                      ],
                    )
                  : Container(
                      color: Colors.white,
                      padding:
                          const EdgeInsets.only(left: 4, right: 4, top: 10),
                      child: LayoutBuilder(builder: (context, constraint) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            InkWell(
                              onTap: () {
                                Navigator.pushNamed(
                                    context, RoutesNames.SecondPage,
                                    arguments: todayDate);
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Expanded(
                                    child: Card(
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(20)),
                                        elevation: 4,
                                        child: DateCard(
                                          key: widget.key,
                                          title: DateFormat('EEEE')
                                              .format(todayDate!)
                                              .toString(),
                                          value: DateFormat('MM-y')
                                              .format(todayDate!)
                                              .toString(),
                                        )),
                                  ),
                                ],
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                Navigator.pushNamed(
                                    context, RoutesNames.SecondPage,
                                    arguments: tomorrowDate);
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: Card(
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(20)),
                                        elevation: 4,
                                        child: DateCard(
                                          key: widget.key,
                                          title: DateFormat('EEEE')
                                              .format(tomorrowDate!)
                                              .toString(),
                                          value: DateFormat('MM-y')
                                              .format(tomorrowDate!)
                                              .toString(),
                                        )),
                                  ),
                                ],
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                Navigator.pushNamed(
                                    context, RoutesNames.SecondPage,
                                    arguments: afterTomorrowDate);
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Expanded(
                                    child: Card(
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(20)),
                                        elevation: 4,
                                        child: DateCard(
                                          key: widget.key,
                                          title: DateFormat('EEEE')
                                              .format(afterTomorrowDate!)
                                              .toString(),
                                          value: DateFormat('MM-y')
                                              .format(afterTomorrowDate!)
                                              .toString(),
                                        )),
                                  ),
                                ],
                              ),
                            ),
                            const Spacer(),
                            Expanded(
                              flex: 2,
                              child: Container(
                                padding: const EdgeInsets.all(2.0),
                                child: Image(
                                    width: ScreenUtil().setWidth(500),
                                    fit: BoxFit.contain,
                                    image:
                                        AssetImage(imagesNames['firstPage']!)),
                              ),
                            )
                          ],
                        );
                      }))));
    }));
  }
}
