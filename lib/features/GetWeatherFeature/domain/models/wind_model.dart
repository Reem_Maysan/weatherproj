class WindModel {
  double degree;
  double gust;
  double speed;

  WindModel({
    required this.speed,
    required this.degree,
    required this.gust,
  });

  factory WindModel.fromJson(Map<String, dynamic> json) {
    return WindModel(
        speed: json['speed'] * 1.0,
        degree: json['deg'] * 1.0,
        gust: json['gust'] * 1.0);
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['speed'] = speed;
    data['deg'] = degree;
    data['gust'] = gust;
    return data;
  }

  static List<WindModel> fromJsonList(List<dynamic> items) {
     List<WindModel> list = [];
    for (var x in items) {
      list.add(WindModel.fromJson(x));
    }
    return list;
  }
}
