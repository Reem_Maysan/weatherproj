import 'package:flutter/material.dart';
import 'package:weather_project/app/presentation/app.dart';
import 'package:weather_project/injection.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await iniGetIt();
  runApp(App());
}
