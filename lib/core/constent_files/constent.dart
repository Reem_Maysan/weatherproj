const String BaseUrl = 'http://api.openweathermap.org';

const String Description = "Description";
const String CityName = "CityName";
const String ImageMain = "ImageMain";
const String CurrentDate = "CurrentDate";
const String Humidity = 'Humidity';
const String Temperature = 'Temperature';
const String Wind = 'Wind';

const String APP_LANGUAGE = "App_language";
