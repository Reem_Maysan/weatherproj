import 'dart:convert';
import 'package:dio/dio.dart'; 
import 'package:weather_project/core/Response/failure.dart';
import 'package:weather_project/core/Response/response_model.dart';
import 'package:weather_project/features/GetWeatherFeature/data/http/Ihttp_helper.dart';
import 'package:weather_project/features/GetWeatherFeature/domain/models/basic_response_model.dart';
import 'package:weather_project/injection.dart';

class WeatherHttpHelper implements WeatherIHttpHelper {
  Dio? dio = sl<Dio>();

  WeatherHttpHelper(Dio this.dio) {
    dio!.interceptors.add(
      LogInterceptor(
        request: true,
        responseBody: true,
        requestBody: true,
      ),
    );
  }

  @override
  Future<ResponseModel> getForcastDataList() async {
    try {
      final response = await dio!.get(
          '/data/2.5/forecast?id=292223&appid=ec6d6bdf4fbce96b933adc72e802fbbb',
          options: Options(
            contentType: Headers.formUrlEncodedContentType,
          ));

      final result = json.decode(response.data);
      if (response.statusCode == 200 || response.statusCode == 201) {
        final BasicResponseModel basicResponseModel =
            BasicResponseModel.fromJson(result); 
        return ResponseModel(basicResponseModel);
      } else {
        return ResponseModel(Failure(message: result['message']));
      }
    } on DioError catch (e) { 
      return ResponseModel(Failure(message: e.message));
    }
  } 

}
