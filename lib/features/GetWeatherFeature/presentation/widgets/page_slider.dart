import 'package:flutter/material.dart';
import 'package:weather_project/features/GetWeatherFeature/presentation/widgets/weather_card.dart';

class PageSlider extends StatefulWidget {
  final List<WeatherCard>? weatherCardsList;
  final double? height;
  const PageSlider({Key? key, this.weatherCardsList, this.height})
      : super(key: key);

  @override
  _PageSliderState createState() => _PageSliderState();
}

class _PageSliderState extends State<PageSlider> {
  PageController pageController = PageController(viewportFraction: 0.6);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: SizedBox(
        height: widget.height,
        child: ListView.builder(
          padding: const EdgeInsets.only(left: 5, right: 5, bottom: 5),
          itemCount: widget.weatherCardsList != null
              ? widget.weatherCardsList!.length
              : 0,
          controller: pageController,
          scrollDirection: Axis.horizontal,
          physics: const PageScrollPhysics(),
          itemBuilder: (_, int i) {
            return Card(
                elevation: 2,
                child: widget.weatherCardsList != null
                    ? widget.weatherCardsList![i]
                    : Container());
          },
        ),
      ),
    );
  }
}
