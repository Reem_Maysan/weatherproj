import 'package:equatable/equatable.dart'; 

class AppState extends Equatable {
  final int? appLanguage;
  const AppState({
    this.appLanguage,
  });

  @override
  List<Object?> get props => [appLanguage];
}

class Loading extends AppState {
  @override
  List<Object> get props => [];
}
