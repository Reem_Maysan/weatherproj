import 'package:weather_project/core/Response/failure.dart';

class ResponseModel<T> {
  Failure? failure;
  T? model;
  bool get hasError => failure != null;
  ResponseModel(dynamic response) {
    if (response is Failure) {
      failure = response;
    } else {
      model = response;
    }
  }
}
