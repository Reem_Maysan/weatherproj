class MainModel {
  double temperature;
  double feelsLike;
  double temperatureMin;
  double temperatureMax;
  double pressure;
  double seaLevel;
  double groundLevel;
  double humidity;
  double temperatureKf;

  MainModel(
      {required this.temperature,
      required this.feelsLike,
      required this.groundLevel,
      required this.humidity,
      required this.pressure,
      required this.seaLevel,
      required this.temperatureKf,
      required this.temperatureMax,
      required this.temperatureMin});

  factory MainModel.fromJson(Map<String, dynamic> json) {
    return MainModel(
        temperature: json['temp'] * 1.0,
        feelsLike: json['feels_like'] * 1.0,
        groundLevel: json['grnd_level'] * 1.0,
        pressure: json['pressure'] * 1.0,
        seaLevel: json['sea_level'] * 1.0,
        temperatureKf: json['temp_kf'] * 1.0,
        temperatureMax: json['temp_max'] * 1.0,
        temperatureMin: json['temp_min'] * 1.0,
        humidity: json['humidity'] * 1.0);
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['temp'] = temperature;
    data['feels_like'] = feelsLike;
    data['grnd_level'] = groundLevel;
    data['pressure'] = pressure;
    data['sea_level'] = seaLevel;
    data['temp_kf'] = temperatureKf;
    data['temp_max'] = temperatureMax;
    data['temp_min'] = temperatureMin;
    data['humidity'] = humidity;
    return data;
  }

  static List<MainModel> fromJsonList(List<dynamic> items) {
     List<MainModel> list = [];
    for (var x in items) {
      list.add(MainModel.fromJson(x));
    }
    return list;
  }
}
