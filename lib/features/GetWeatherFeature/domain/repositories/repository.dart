 
import 'package:weather_project/core/Response/returned_response.dart';
import 'package:weather_project/core/preference/Ipreference_helper.dart';
import 'package:weather_project/features/GetWeatherFeature/data/http/Ihttp_helper.dart';
import 'package:weather_project/features/GetWeatherFeature/domain/entities/forcast_entity.dart';
import 'package:weather_project/features/GetWeatherFeature/domain/models/basic_response_model.dart';
import 'package:weather_project/features/GetWeatherFeature/domain/models/forcast_model.dart';
import 'package:weather_project/features/GetWeatherFeature/domain/repositories/Irepository.dart';

class WeatherRepository implements WeatherIRepository {
  WeatherIHttpHelper? weatherIHttpHelper;
  IPrefsHelper? iPrefsHelper;
  WeatherRepository(this.weatherIHttpHelper, this.iPrefsHelper);

  @override
  Future<ReturnedResponse> getForcastDataList(String? date) async {  
    if (await iPrefsHelper!.isStored(date)) {
      ForcastEntity forcastEntity = await iPrefsHelper!.getData();
      return ReturnedResponse(forcastEntity);
    } else {
      final responseModel = await weatherIHttpHelper!.getForcastDataList();
      return ReturnedResponse(responseModel);
    }
  }

  @override
  Future<void> saveData(BasicResponseModel basicResponseModel,
      ForcastModel forcastModel, String date) async {
    await iPrefsHelper!.saveData(ForcastEntity(
        city: basicResponseModel.city,
        mainModel: forcastModel.mainModel,
        weatherModel: forcastModel.weatherModel.first,
        windModel: forcastModel.windModel,
        currentDate: date));
  }
}
