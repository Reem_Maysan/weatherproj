import 'package:equatable/equatable.dart'; 
import 'package:weather_project/features/GetWeatherFeature/presentation/widgets/weather_card.dart';

abstract class WeatherEvents extends Equatable {}

class FetchForcastDataList extends WeatherEvents {
  final String date;
  FetchForcastDataList({required this.date});

  @override
  List<Object?> get props => [date];
}

class InitWeatherCardsList extends WeatherEvents {
  final List<WeatherCard>? weatherCardsList;
  InitWeatherCardsList({this.weatherCardsList});

  @override
  List<Object?> get props => [weatherCardsList];
}
